// init your app to let angular recognize the tags on your html.
var RegApp = angular.module("RegApp", []);

(function() {
    var RegCtrl = function() {
        var ctrl = this;

        //initial values
        ctrl.myname="wsy";
        ctrl.age = 21;
        ctrl.pageColor = "cyan";
    };
    RegApp.controller("RegCtrl", [RegCtrl]);
}) ();