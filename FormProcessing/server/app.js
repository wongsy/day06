/**
 * Created by s25854 on 18/10/2016.
 */
var express = require("express");
var app = express();

const PORT = process.argv[2] || 3000;
app.use(express.static(__dirname + "/../client"));

// Starts the server on localhost (default)
app.listen(PORT, function(){
    console.log("Server listening on: http://localhost:%s", PORT);
});

